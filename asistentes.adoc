== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Fernandez Isa, Daniel
* Gallardo Harillo, José
* Luque Giráldez, José Rafael
* Martínez Bernal, Manuel
* Rahali, Rim.
* Rodríguez Barrera, Antonio Ángel
* Vega Zambrana, Adrián
* Viegas Peñalosa, Pedro
* Zamora Pérez, Manuel
=======


=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Peugeot 508 (5 plazas)

* Rafael Luque
* Manuel Martínez
* Antonio Angel Rodriguez
* Adrián Vega
* Daniel Fernández
=======

==== Ferrari Rojo (12 plazas)

* Manuel Zamora 
* Rim Rahali

==== Triciclo 2007 (1 plaza)

* Pedro Viegas

==== Miraidon 678 (3 plazas)

* José Gallardo

==== Formula 1 (1 plaza) 

* Isaac Gutierrez
